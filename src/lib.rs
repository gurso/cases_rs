pub fn from_pascal(s: &str) -> Vec<&str> {
    let mut list: Vec<&str> = vec![];
    let mut i = 0;
    let mut p = 0;
    
    for c in s.chars() {
        if c == c.to_ascii_uppercase() {
            let w = s.get(p..i).unwrap();

            if w.len() > 0 {
                list.push(w)
            }
            p = i
        }
        i = i + 1
    }
    let w = s.get(p..).unwrap();
    list.push(w);
    list
}
pub fn from_kebab(s: &str) -> Vec<&str> {
    s.split("-").collect()
}
pub fn from_snake(s: &str) -> Vec<&str> {
    s.split("_").collect()
}

pub fn ucf(from: &str) -> String {
    let first = from.get(0..1).unwrap().to_uppercase();
    let end = from.get(1..).unwrap();
    format!("{}{}", first, end)
}
pub fn lcf(from: &str) -> String {
    let first = from.get(0..1).unwrap().to_lowercase();
    let end = from.get(1..).unwrap();
    format!("{}{}", first, end)
}

// From pascal
pub fn pascal_to_snake(s: &str) -> String {
    let v = from_pascal(s);
    let mut r: Vec<String> = vec![];
    for w in v {
        r.push(w.to_lowercase());
    }
    r.join("_")
}
pub fn pascal_to_kebab(s: &str) -> String {
    let v = from_pascal(s);
    let mut r: Vec<String> = vec![];
    for w in v {
        r.push(w.to_lowercase());
    }
    r.join("-")
}
pub fn pascal_to_camel(s: &str) -> String {
    lcf(s)
}

// From camel
pub fn camel_to_snake(s: &str) -> String {
    pascal_to_snake(s)
}
pub fn camel_to_kebab(s: &str) -> String {
    pascal_to_kebab(s)
}
pub fn camel_to_pascal(s: &str) -> String {
    ucf(s)
}

// from snake
pub fn snake_to_pascal(s: &str) -> String {
    let v = from_snake(s);
    let mut r: Vec<String> = vec![];
    for w in v {
        r.push(ucf(w));
    }
    r.join("")
}
pub fn snake_to_kebab(s: &str) -> String {
    let v = from_snake(s);
    let mut r: Vec<String> = vec![];
    for w in v {
        r.push(w.to_string());
    }
    r.join("-")
}
pub fn snake_to_camel(s: &str) -> String {
    lcf(&snake_to_pascal(s))
}

// from kebab
pub fn kebab_to_pascal(s: &str) -> String {
    let v = from_kebab(s);
    let mut r: Vec<String> = vec![];
    for w in v {
        r.push(ucf(w));
    }
    r.join("")
}
pub fn kebab_to_snake(s: &str) -> String {
    let v = from_kebab(s);
    let mut r: Vec<String> = vec![];
    for w in v {
        r.push(w.to_string());
    }
    r.join("_")
}
pub fn kebab_to_camel(s: &str) -> String {
    lcf(&kebab_to_pascal(s))
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ucf_test() {
        let v = ucf("ucf");
        assert_eq!(v, "Ucf");
    }

    #[test]
    fn from_pascal_test() {
        let v = from_pascal("PascalCaseTest");
        assert_eq!(v, vec!["Pascal", "Case", "Test"]);
    }

    #[test]
    fn from_camel_test() {
        let v = from_pascal("pascalCaseTest");
        assert_eq!(v, vec!["pascal", "Case", "Test"]);
    }
}


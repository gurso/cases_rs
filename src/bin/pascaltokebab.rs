use std::env;
use cases::pascal_to_kebab;

fn main() {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);
    for arg in args {
        println!("{}", pascal_to_kebab(&arg));
    }
}



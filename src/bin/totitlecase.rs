use std::env;
use cases::ucf;

fn main() {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);
    for arg in args {
        print!("{} ", ucf(&arg));
    }
}


